<?php
/**
 * AgaveFileListing.php
 * @author mrhanlon
 */
class AgaveFileListing {

  private $client;
  private $systemId;
  private $filePath;

  /**
   * @param string $client an instance of APIClient
   */
  function __construct($client, $systemId, $filePath = '/') {
    $this->client = $client;
    $this->systemId = $systemId;
    $this->filePath = $filePath;
  }

  public function getList(){
    try {
      $apiResp = $this->client->callAPI($this->buildUrl());
      if ($apiResp->status == 'success') {
        return $apiResp->result;
      } else {
        throw new Exception($apiResp->message);
      }
    } catch (Exception $e) {
      watchdog('agave_files'
        , t('An error occurred while fetching %filePath from system %systemId: ', array('%filePath' => $this->filePath, '%system' =>$this->systemId)) . $e->getMessage()
        , NULL
        , WATCHDOG_ERROR);
    }
    return FALSE;
  }

  function buildUrl() {
    $url = '/files/v2/listings';
    if ($this->systemId) {
      $url = $url . '/system/' . $this->systemId;
    }
    return $url . $this->filePath;
  }
}
