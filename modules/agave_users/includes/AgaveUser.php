<?php
/**
 * AgaveUser.php
 * @author mrhanlon
 */
class AgaveUser {

  private $username;
  private $email;
  private $firstName;
  private $lastName;
  private $mobilePhone;
  private $phone;
  private $status;
  private $createTime;

  public function __construct($username, $email, $firstName = '', $lastName = '', $mobilePhone = '', $phone = '', $status = 'Active', $createTime = '') {
    $this->setUsername($username);
    $this->setEmail($email);
    $this->setFirstName($firstName);
    $this->setLastName($lastName);
    $this->setMobilePhone($mobilePhone);
    $this->setPhone($phone);
    $this->setStatus($status);
    $this->setCreateTime($createTime);
  }

  public function getUsername() {
    return $this->username;
  }
  public function setUsername($username) {
    $this->username = $username;
  }

  public function getEmail() {
    return $this->email;
  }
  public function setEmail($email) {
    $this->email = $email;
  }

  public function getFirstName() {
    return $this->firstName;
  }
  public function setFirstName($firstName) {
    $this->firstName = $firstName;
  }

  public function getLastName() {
    return $this->lastName;
  }
  public function setLastName($lastName) {
    $this->lastName = $lastName;
  }

  public function getMobilePhone() {
    return $this->mobilePhone;
  }
  public function setMobilePhone($mobilePhone) {
    $this->mobilePhone = $mobilePhone;
  }

  public function getPhone() {
    return $this->phone;
  }
  public function setPhone($phone) {
    $this->phone = $phone;
  }

  public function getStatus() {
    return $this->status;
  }
  public function setStatus($status) {
    $this->status = $status;
  }

  public function getCreateTime() {
    return $this->createTime;
  }
  public function setCreateTime($createTime) {
    $this->createTime = $createTime;
  }

  public function isNew() {
    return $this->getCreateTime() === '';
  }

  /**
   * @param $updatePassword new password for a password change
   * @throws Exception if the save fails
   */
  public function save($updatePassword = '') {

    $data = array(
      'username'      => $this->getUsername()
    , 'email'         => $this->getEmail()
    , 'first_name'    => $this->getFirstName()
    , 'last_name'     => $this->getLastName()
    , 'phone'         => $this->getPhone()
    , 'mobile_phone'  => $this->getMobilePhone()
    );

    if ($updatePassword) {
      $data['password'] = $updatePassword;
    }

    $apiToken = variable_get('agave_tenant_token', '');
    $client = new APIClient($apiToken, variable_get('agave_tenant_base_url', ''));

    $resourcePath = '/profiles/v2/';
    $method = 'POST';
    if (! $this->isNew()) {
      $resourcePath = $resourcePath . $this->getUsername() . '/';
      $method = 'PUT';
    }

    $resp = $client->callAPI($resourcePath, $method, array(), json_encode($data), array());
    if ($resp->status === 'error') {
      throw new Exception($resp->message);
    }
    return $resp;
  }

  /**
   * Find a user by username
   * @param $username the username of the user to locate
   * @return An AgaveUser object or NULL if no user found for $username
   * @throws Exception on an API error
   */
  public static function findUser($username) {
    $apiToken = variable_get('agave_tenant_token', '');
    $client = new APIClient($apiToken, variable_get('agave_tenant_base_url', ''));
    try {
      $resp = $client->callAPI('/profiles/v2/'.$username, 'GET', array(), null, array());
      if ($resp && $resp->status === 'success') {
        $user = new AgaveUser(
          $resp->result->username
        , $resp->result->email
        , $resp->result->first_name
        , $resp->result->last_name
        , $resp->result->mobile_phone
        , $resp->result->phone
        , $resp->result->status
        , $resp->result->create_time
        );
        return $user;
      }
    } catch (Exception $e) {
      error_log($e->getMessage());
      throw new Exception('An error occurred looking up user "'.$username.'"', 0, $e);
    }
  }
}
