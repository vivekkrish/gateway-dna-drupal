<?php

function agave_users_admin(){
  $output = array();
  $output['intro'] = array(
    '#prefix' => '<p>'
    , '#markup' => t('Manage users in the Agave Tenant')
    , '#suffix' => '</p>'
  );
  $output['actions'] = array(
    '#items' => array(
      l('Manage Agave Users', 'admin/config/agave/users/manage')
    )
    , '#theme' => 'item_list'
  );

  $output['configured_systems'] = array();
  return $output;
}

function agave_users_admin_manage_api_users() {

  $output = array();

  $apiToken = variable_get('agave_tenant_token', '');
  if ($apiToken) {

    $output['intro'] = array(
      '#markup' => t('The following users currently exist in the configured Agave Tenant:')
      , '#prefix' => '<p>'
      , '#suffix' => '</p>'
    );

    $client = new APIClient($apiToken, variable_get('agave_tenant_base_url', ''));
    try {
      $apiResp = $client->callAPI('/profiles/v2/', 'GET', array(), null, array('Authorization' => "Bearer $apiToken"));
      if ($apiResp && $apiResp->status === 'success') {
        $rows = array();
        foreach($apiResp->result as $user) {
          $account = user_load_by_name($user->username);
          $remoteUser = user_external_load($user->username);
          $row = array(
            $user->first_name
            , $user->last_name
            , $account ? theme('username', array('account' => $account)) : $user->username
            , $user->email
            , $user->mobile_phone
          );
          if ($account) {
            if ($remoteUser) {
              $row[] = '<span class="label label-success">'.t('Active').'</span>';
            } else {
              $row[] = l(t('Set Agave Authmaps'), 'admin/config/agave/users/manage/authmap/'.$account->uid, array('attributes' => array('class' => array('btn btn-default'))));
            }
          } else {
            $row[] = '<span class="label label-warning">'.t('No local account').'</span>';
          }
          $rows[] = $row;
        }
        $output['users'] = array(
          '#theme' => 'table'
          , '#header' => array(t('First name'), t('Last name'), t('Username'), t('Email'), t('Mobile number'), t('Status'))
          , '#rows' => $rows
        );
      }
    } catch (Exception $e) {
      drupal_set_message(t($e->getMessage()), 'error', FALSE);
    }
  } else {
    drupal_set_message(t('You must configure the Tenant before you can manage users! !link', array('!link' => l('Configure tenant', 'admin/config/agave/tenant'))), 'error');
  }

  return $output;
}

function agave_users_admin_set_agave_authmaps($account) {
  user_set_authmaps($account, array('authname_agave_users' => $account->name));
  drupal_set_message(t('Agave User Authmaps set for user: %name.', array('%name' => $account->name)), 'status', FALSE);
  drupal_goto('admin/config/agave/users/manage');
}