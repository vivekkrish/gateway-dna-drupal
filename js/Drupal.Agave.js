/*globals jQuery, Drupal, SwaggerApi, ApiKeyAuthorization*/
(function ( window, $, undefined ) {
  'use strict';
  var log = Function.prototype.bind.call(console.log, console);

  // polyfill
  (function () {
    function CustomEvent ( event, params ) {
      params = params || { bubbles: false, cancelable: false, detail: undefined };
      var evt = document.createEvent( 'CustomEvent' );
      evt.initCustomEvent( event, params.bubbles, params.cancelable, params.detail );
      return evt;
     };

    CustomEvent.prototype = window.Event.prototype;

    window.CustomEvent = CustomEvent;
  })();

  Drupal.behaviors.agave = {
    attach: function( context, settings ) {

      // Only attach to initial page load
      if (context !== document) {
        return;
      }

      var Agave = window.Agave || {};
      Agave.baseUrl = settings.agave.baseURL;
      Agave.token = settings.agave.token;

      /**
       * Initializes the Agave.api object. Initialization will fail if the
       * Agave.token is null.
       * @returns A promise for async initialization.
       */
      Agave.initialize = function() {
        var deferred = $.Deferred();

        if ( Agave.token ) {
          Agave.api = new SwaggerClient({
            url: window.location.origin + settings.basePath + 'agave/resources',
            useJQuery: true,
            success: function() {
              if ( Agave.api.ready === true ) {
                deferred.resolve( Agave );
              }
            },
            failure: function( error ) {
              deferred.reject( error );
            }
          });
          Agave.api.clientAuthorizations.add( 'Authorization', new SwaggerClient.ApiKeyAuthorization( 'Authorization', 'Bearer ' + Agave.token.accessToken, 'header' ) );
        } else {
          deferred.reject( 'Agave token uninitialized.' );
        }

        return deferred.promise();
      };

      Agave.initialize().done(function() {
        log( 'Agave API ready!' );
        window.dispatchEvent( new CustomEvent( 'Agave::ready' ) );
      });

      window.Agave = Agave;
    }
  };
})( window, jQuery );
