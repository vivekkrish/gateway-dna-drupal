<?php
/**
 * AgaveToken.php
 * @author mrhanlon
 */
class AgaveToken {

  private $accessToken;
  private $refreshToken;
  private $expiresIn;
  private $tokenType;
  private $created;

  public function __construct($accessToken, $refreshToken, $expiresIn, $tokenType, $created){
    $this->setAccessToken($accessToken);
    $this->setRefreshToken($refreshToken);
    $this->setExpiresIn($expiresIn);
    $this->setTokenType($tokenType);
    $this->setCreated($created);
  }

  public function getAccessToken() {
    return $this->accessToken;
  }

  public function setAccessToken($accessToken) {
    $this->accessToken = $accessToken;
  }

  public function getRefreshToken() {
    return $this->refreshToken;
  }

  public function setRefreshToken($refreshToken) {
    $this->refreshToken = $refreshToken;
  }

  public function getExpiresIn() {
    return $this->expiresIn;
  }

  public function setExpiresIn($expiresIn) {
    $this->expiresIn = $expiresIn;
  }

  public function getTokenType() {
    return $this->tokenType;
  }

  public function setTokenType($tokenType) {
    $this->tokenType = $tokenType;
  }

  public function getCreated() {
    return $this->created;
  }

  public function setCreated($created) {
    $this->created = $created;
  }

  public function isValid() {
    return time() - $this->created < $this->expiresIn;
  }

  public function isExpired() {
    return ! $this->isValid();
  }

  public function json() {
    $obj = new stdClass();
    $obj->tokenType = $this->tokenType;
    $obj->expiresIn = $this->expiresIn;
    $obj->refreshToken = '';
    $obj->accessToken = $this->accessToken;
    $obj->created = $this->created;
    return $obj;
  }

  public function refresh() {
    $client = new APIClient('', variable_get('agave_tenant_base_url', ''));
    $consumer_key = variable_get('agave_tenant_consumer_key', '');
    $consumer_secret = variable_get('agave_tenant_consumer_secret', '');
    $consumer_base64 = base64_encode("$consumer_key:$consumer_secret");
    $body = http_build_query(array(
      'refresh_token' => $this->refreshToken
      , 'grant_type' => 'refresh_token'
      , 'scope' => 'PRODUCTION'
    ));
    try {
      $apiResp = $client->callAPI('/token', 'POST', array(), $body, array(
        'Authorization' => "Basic $consumer_base64"
        , 'Content-type' => 'application/x-www-form-urlencoded'
      ));
    } catch (Exception $e) {
      throw new Exception('Refresh token failed: ' . $e->getMessage(), 0, $e);
    }

    if (property_exists($apiResp, 'error')) {
      throw new Exception('Refresh token failed: ' . $apiResp->error_description);
    } else {
      $this->setAccessToken($apiResp->access_token);
      $this->setRefreshToken($apiResp->refresh_token);
      $this->setExpiresIn($apiResp->expires_in);
      $this->setTokenType($apiResp->token_type);
      $this->setCreated(time());
    }
  }

  /**
   * Load a persisted token from the database by User ID
   * @param
   *   $userId The ID of the User whose token to load
   * @return
   *   The token or FALSE if a token has not been previously presisted
   *   for the user
   */
  public static function newToken($grant_type, $options = array()) {
    $client = new APIClient('', variable_get('agave_tenant_base_url', ''));
    $consumer_key = variable_get('agave_tenant_consumer_key', '');
    $consumer_secret = variable_get('agave_tenant_consumer_secret', '');
    $consumer_base64 = base64_encode("$consumer_key:$consumer_secret");
    $query_args = array();
    $query_args['grant_type'] = $grant_type;

    if (isset($options['scope'])) {
      $query_args['scope'] = $options['scope'];
    } else {
      $query_args['scope'] = 'PRODUCTION';
    }

    $token_url = '/token';
    if ($grant_type === 'password') {
      $query_args['username'] = $options['name'];
      $query_args['password'] = $options['pass'];
    } else if ($grant_type === 'authorization_code') {
      $token_url = '/oauth2' . $token_url;
      $query_args['code'] = $options['code'];

      if (isset($options['redirect'])) {
        $query_args['redirect_uri'] = $options['redirect'];
      } else {
        $query_args['redirect_uri'] = url('agave/oauth2', array('absolute' => TRUE));
      }
    } else {
      throw new Exception('Invalid grant type: ' + $grant_type);
    }

    $body = http_build_query($query_args);
    try {
      $apiResp = $client->callAPI($token_url, 'POST', array(), $body, array(
        'Authorization' => "Basic $consumer_base64"
        , 'Content-type' => 'application/x-www-form-urlencoded'
      ));
    } catch (Exception $e) {
      error_log($e->getMessage());
      throw new Exception('Unable to obtain a valid token', 0, $e);
    }

    if (property_exists($apiResp, 'access_token') && $apiResp->access_token) {
      // got a token!
      $token = new AgaveToken(
        $apiResp->access_token
      , $apiResp->refresh_token
      , $apiResp->expires_in
      , $apiResp->token_type
      , time()
      );
      return $token;
    } else {
      return NULL;
    }
  }
}
